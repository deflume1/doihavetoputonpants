'use strict';

/**
 * This file contains a map of messages used by the skill.
 */

const WELCOME = 'Welcome to the Do I Have To Wear Pants Skill!';

const WHAT_DO_YOU_WANT = 'Ask me Do I Have To Wear Pants?';

const NOTIFY_MISSING_PERMISSIONS = 'You need to enable Location permissions in the Alexa app, first.';

const NO_ADDRESS = 'It looks like you dont have an address set. You can set your address in the Alexa app.';

const ERROR = 'Uh Oh. Looks like something went wrong.';

const LOCATION_FAILURE = 'There was an error with the Device Address API. Please try again.';

const GOODBYE = 'Bye! Thanks for using the Do I Have To Wear Pants Skill!';

const UNHANDLED = 'This skill doesnt support that. Please ask something else.';

const HELP = 'You can use this skill by asking something like: Do I Have To Wear Pants?';

const UNSUPPORTED_COUNTRY = 'Sorry, the  Do I Have To Wear Pants Skill only supports users in the United States right now.';

module.exports = {
    'WELCOME': WELCOME,
    'WHAT_DO_YOU_WANT': WHAT_DO_YOU_WANT,
    'NOTIFY_MISSING_PERMISSIONS': NOTIFY_MISSING_PERMISSIONS,
    'NO_ADDRESS': NO_ADDRESS,
    'ERROR': ERROR,
    'LOCATION_FAILURE': LOCATION_FAILURE,
    'GOODBYE': GOODBYE,
    'UNHANDLED': UNHANDLED,
    'HELP': HELP,
    'UNSUPPORTED_COUNTRY': UNSUPPORTED_COUNTRY
};
