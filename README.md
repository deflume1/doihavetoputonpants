# Do I Have To Put On Pants?

Checks the weather near you and tells you whether you need to put on pants.  Maybe you do, maybe you don't.  You probably don't.

This makes use of the following:

* https://www.openweathermap.org/api
* https://github.com/dirkbonhomme/js-cache - @DirkBonhomme
* https://github.com/KwabenBerko/OpenWeatherMap-Node - @KwabenBerko
* https://github.com/expressjs/express - @expressjs
* https://github.com/expressjs/body-parser - @expressjs
* https://github.com/jquery/jquery - @jquery
* https://github.com/nodejs/node - @nodejs

I host it at https://doihavetoputonpants.com . 

I also created an Alexa skill, Do I Have To Wear Pants, and I put the code for the associated lambda function in the lambda/ directory.

Right now, it supports US zip codes.  Tried to add UK/CA support, but OpenWeatherMap doesn't quite support them yet.  If you're interested in adding support for other zipcodes or search methods, open a PR!
