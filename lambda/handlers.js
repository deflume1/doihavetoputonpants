'use strict';

/**
 * This class contains all handler function definitions
 * for the various events that we will be registering for.
 * For an understanding of how these Alexa Skill event objects
 * are structured refer to the following documentation:
 * https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/alexa-skills-kit-interface-reference
 */

// Internal imports
const AlexaDeviceAddressClient = require('./location');
const Intents = require('./intents');
const Events = require('./events');
const Messages = require('./messages');
const axios = require('axios');

/**
 * Another Possible value if you only want permissions for the country and postal code is:
 * read::alexa:device:all:address:country_and_postal_code
 * Be sure to check your permissions settings for your skill on https://developer.amazon.com/
 */
const COUNTRY_AND_POSTCODE_PERMISSION = 'read::alexa:device:all:address:country_and_postal_code';

const PERMISSIONS = [COUNTRY_AND_POSTCODE_PERMISSION];

/**
 * This is the handler for the NewSession event.
 * Refer to the  Events.js file for more documentation.
 */
const newSessionRequestHandler = function() {
    //console.info('Starting newSessionRequestHandler()');

    if(this.event.request.type === Events.LAUNCH_REQUEST) {
        this.emit(Events.LAUNCH_REQUEST);
    } else if (this.event.request.type === 'IntentRequest') {
        this.emit(this.event.request.intent.name);
    }

    //console.info('Ending newSessionRequestHandler()');
};

/**
 * This is the handler for the LaunchRequest event. Refer to
 * the Events.js file for more documentation.
 */
const launchRequestHandler = function() {
    //console.info('Starting launchRequestHandler()');
    this.emit(':ask', Messages.WELCOME + Messages.WHAT_DO_YOU_WANT, Messages.WHAT_DO_YOU_WANT);
    //console.info('Ending launchRequestHandler()');
};

const getWeatherMessage = function(units, rawTemp, rawWind, windchill, realTemp) {
	let windspeedUnit = "miles per hour";
	let tempUnit = "fahrenheit";

	if(units == "metric") {
		windspeedUnit = "kilometers per hour";
		tempUnit = "celsius";
	}
	
	let weatherMessage = "OpenWeatherMap says that it's " + rawTemp + " degrees " + tempUnit + " outside";
	if(windchill && windchill > 0) {
		weatherMessage += " but there's a wind chill of " + windchill + " degrees " + tempUnit + ", so it feels like it's " + realTemp + " degrees " + tempUnit + ". ";
	} else {
		weatherMessage += ".";
	}

	weatherMessage += "The current windspeed is " + rawWind + " " + windspeedUnit + ".";

	return weatherMessage;
};

const processResponse = function(alexa, data) {
	const rawTemp = Number(data.temp);
    const rawWind = Number(data.wind);
    const windchill = Number(data.windChill);
    const realTemp = Number(data.realTemp);
	const units = data.units;

	const weatherMessage = getWeatherMessage(units, rawTemp, rawWind, windchill, realTemp);

	if((units === 'imperial' && realTemp <= 32) || (units === 'metric' && realTemp <= 0)) { // it's cold, wear pants
		let opinionMessage = "Yeah, you should probably wear pants. "
		alexa.emit(':tell', opinionMessage + weatherMessage);
	} else if((units === 'imperial' && realTemp <= 50) || (units === 'metric' && realTemp <= 10)) { // shorts only for the bold
		let opinionMessage = "Hmmm, you might want to wear pants, if you're delicate. "
		alexa.emit(':tell', opinionMessage + weatherMessage);
    } else if((units === 'imperial' && realTemp <= 70) || (units === 'metric' && realTemp <= 21.11)) { // I personally disapprove if you're not wearing shorts
		let opinionMessage = "If you decide to wear pants, I question your commitment to comfort. "
		alexa.emit(':tell', opinionMessage + weatherMessage);
    } else { // if you're wearing pants, you're a fool
		let opinionMessage = "Only a fool would wear pants on a day as glorious as this one. "
		alexa.emit(':tell', opinionMessage + weatherMessage);
	}
};

/**
 * This is the handler for our custom doIHaveToPutOnPants intent.
 * Refer to the Intents.js file for documentation.
 */
const doIHaveToPutOnPantsHandler = function() {
    //console.info('Starting doIHaveToPutOnPantsHandler()');

    const apiAccessToken= this.event.context.System.apiAccessToken;
    const deviceId = this.event.context.System.device.deviceId; 
    const apiEndpoint = this.event.context.System.apiEndpoint;

    const alexaDeviceAddressClient = new AlexaDeviceAddressClient(apiEndpoint, deviceId, apiAccessToken);
    let deviceAddressRequest = alexaDeviceAddressClient.getCountryAndPostalCode();

    deviceAddressRequest.then((addressResponse) => {
        switch(addressResponse.statusCode) {
        case 200: {
            //console.log('Address successfully retrieved, now responding to user.');
            const countryCode = addressResponse.address.countryCode;

            if(!countryCode || !(countryCode.trim().toLowerCase() === 'us')) {
                this.emit(':tell', Messages.UNSUPPORTED_COUNTRY);
                break;
            }

            const postalCode = addressResponse.address.postalCode;
            
            const reqstr = 'https://doihavetoputonpants.com/welldoi/';
            axios.post(reqstr, {zipcode: postalCode, country: countryCode})
                  .then ( (response) => {
                  //console.log(response.data);
				  processResponse(this, response.data);
            });
            break;
        }
        case 204: {
            // This likely means that the user didn't have their address set via the companion app.
            //console.log('Successfully requested from the device address API, but no address was returned.');
            this.emit(':tell', Messages.NO_ADDRESS);
            break;
        }
        case 403: {
            //console.log('The apiAccessToken token we had wasnt authorized to access the users address.');
            this.emit(':tellWithPermissionCard', Messages.NOTIFY_MISSING_PERMISSIONS, PERMISSIONS);
            break;
        }
        default: {
            this.emit(':ask', Messages.LOCATION_FAILURE, Messages.LOCATION_FAILURE);
        }}

        //console.info('Ending doIHaveToPutOnPantsHandler()');
    });

    deviceAddressRequest.catch((error) => {
        this.emit(':tell', Messages.ERROR);
        //console.error(error);
        //console.info('Ending doIHaveToPutOnPantsHandler()');
    });
};

/**
 * This is the handler for the SessionEnded event. Refer to
 * the Events.js file for more documentation.
 */
const sessionEndedRequestHandler = function() {
    //console.info('Starting sessionEndedRequestHandler()');
    this.emit(':tell', Messages.GOODBYE);
    //console.info('Ending sessionEndedRequestHandler()');
};

/**
 * This is the handler for the Unhandled event. Refer to
 * the Events.js file for more documentation.
 */
const unhandledRequestHandler = function() {
    //console.info('Starting unhandledRequestHandler()');
    this.emit(':ask', Messages.UNHANDLED, Messages.UNHANDLED);
    //console.info('Ending unhandledRequestHandler()');
};

/**
 * This is the handler for the Amazon help built in intent.
 * Refer to the Intents.js file for documentation.
 */
const amazonHelpHandler = function() {
    //console.info('Starting amazonHelpHandler()');
    this.emit(':ask', Messages.HELP, Messages.HELP);
    //console.info('Ending amazonHelpHandler()');
};

/**
 * This is the handler for the Amazon cancel built in intent.
 * Refer to the Intents.js file for documentation.
 */
const amazonCancelHandler = function() {
    //console.info('Starting amazonCancelHandler()');
    this.emit(':tell', Messages.GOODBYE);
    //console.info('Ending amazonCancelHandler()');
};

/**
 * This is the handler for the Amazon stop built in intent.
 * Refer to the Intents.js file for documentation.
 */
const amazonStopHandler = function() {
    //console.info('Starting amazonStopHandler()');
    this.emit(':tell', Messages.GOODBYE);
    //console.info('Ending amazonStopHandler()');
};


const handlers = {};
// Add event handlers
handlers[Events.NEW_SESSION] = newSessionRequestHandler;
handlers[Events.LAUNCH_REQUEST] = launchRequestHandler;
handlers[Events.SESSION_ENDED] = sessionEndedRequestHandler;
handlers[Events.UNHANDLED] = unhandledRequestHandler;

// Add intent handlers
handlers[Intents.DO_I_HAVE_TO_PUT_ON_PANTS] = doIHaveToPutOnPantsHandler;
handlers[Intents.AMAZON_CANCEL] = amazonCancelHandler;
handlers[Intents.AMAZON_STOP] = amazonStopHandler;
handlers[Intents.AMAZON_HELP] = amazonHelpHandler;

module.exports = handlers;
