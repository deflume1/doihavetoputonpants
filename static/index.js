(function($) {
    "use strict";

    /**
     * Called when the mailing list form is submitted.  Submits the email to the mailing list
     * API endpoint, then displays a success/failure message.
     * 
     * @param formElem - The jQuery wrapped form element that was submitted
     */
    const zipLookupSubmit = function(formElem) {
        const zipcode = formElem.find('input[name="zipcode"]').val();
        const country = formElem.find('input[name="country"]:checked').val();
        $.ajax({
            type: "POST",
            url: "/welldoi",
            data: JSON.stringify({ "zipcode" : zipcode, country : country }),
            success: function(data, statusCode, xhr) {
                const rawTemp = Number(data.temp);
                const rawWind = Number(data.wind);
                const windchill = Number(data.windChill);
                const realTemp = Number(data.realTemp);
                const units = data.units;

                let tempUnit;
                let speedUnit;
                if(units === 'metric') {
                    tempUnit = 'C';
                    speedUnit = 'kph';
                } else {
                    tempUnit = 'F';
                    speedUnit = 'mph';
                }

                const rawTempDiv = '<div>Raw Temp: ' + rawTemp + ' &deg;' + tempUnit + '</div>';
                const windChillDiv = '<div>Wind Chill: ' + windchill + ' &deg;' + tempUnit + '</div>';
                const totalTempDiv = '<div>Feels Like: ' + realTemp + ' &deg;' + tempUnit + '</div>';
                const windSpeedDiv = '<div>Wind Speed: ' + rawWind + ' ' + speedUnit + '<D-d></div>';

                let answerH1;
                let imgDiv;
                let explanationDiv;

                if((units === 'imperial' && realTemp <= 32) || (units === 'metric' && realTemp <= 0)) { // it's cold, wear pants
                    answerH1 = "<h1 class='answer'>YES</h1>";
                    imgDiv = "<div><img src='yes.png' alt='You should wear pants' /></div>";
                    explanationDiv = "<div class='explanation'>Yeah, it's cold, probably wear pants</div>";
                } else if((units === 'imperial' && realTemp <= 50) || (units === 'metric' && realTemp <= 10)) { // shorts only for the bold
                    answerH1 = "<h1 class='answer'>MAYBE</h1>";                    
                    imgDiv = "<div><img src='maybe.png' alt='You might want wear pants' /></div>";
                    explanationDiv = "<div class='explanation'><em>*I*</em> might not wear pants, but hey, maybe you're delicate</div>";
                } else if((units === 'imperial' && realTemp <= 70) || (units === 'metric' && realTemp <= 21.11)) { // I personally disapprove if you're not wearing shorts
                    answerH1 = "<h1 class='answer'>NO</h1>";                    
                    imgDiv = "<div><img src='no.png' alt='You don't need to wear pants' /></div>";
                    explanationDiv = "<div class='explanation'>If you're wearing pants, I question your commitment to comfort</div>";
                } else { // if you're wearing pants, you're a fool
                    answerH1 = "<h1 class='answer'>HELL NO</h1>";                                        
                    imgDiv = "<div><img src='hellno.png' alt='Nobody should be wearing pants' /></div>";
                    explanationDiv = "<div class='explanation'>Only a fool would wear pants on such a glorious day</div>";
                }

                const summaryDivs = answerH1 + imgDiv + explanationDiv + "<div class='stats'>" +  rawTempDiv + windChillDiv + totalTempDiv + windSpeedDiv + "</div>";                

                $('.lookupcontent').html(summaryDivs);
            },
            error: function(data, statusCode, xhr) {
                alert("Try a valid US zip code. It's also possible we went over our API limits, try again later, or email chris at deflumeri dot com.  Unfortunately, no non US support, since OpenWeatherMap is a little broken right now.");
            },
            contentType: 'application/json'
        }); 
    };
    
    // toggle the zip/postal code label
    const toggleZipLabel = function(elem) {
        if (elem.value == 'us') {
            $('label[for=zipcode]').html('ZIP Code:');
        }
        else {
            $('label[for=zipcode]').html('Postal Code:');
        }
    };
    
    // bind email form submit
    $('#zipcodelookup').on('submit', function(e) {
        zipLookupSubmit($(this));
        e.preventDefault();
        return false;
    });

    // on load, make sure we're displaying the right label
    $(window).on('load',
        function() { 
            const elem = $('input[type=radio][name=country]:checked');
            toggleZipLabel(elem[0]);
        }
    ); 

    // bind label toggle
    $('input[type=radio][name=country]').change(
        function() { 
            toggleZipLabel(this);
        }
    );

    
})($);  
