'use strict';

// External imports
const Alexa = require('ask-sdk-v1adapter');

// Local imports
const Handlers = require('./handlers.js');

// Constants
const APP_ID = process.env.APP_ID; // This value would be your Skill ID. You can find this on https://developer.amazon.com/

exports.handler = function (event, context) {
    let alexa = Alexa.handler(event, context);

    alexa.appId = APP_ID;
    alexa.registerHandlers(Handlers);

    //console.log(`Beginning execution for skill with APP_ID=${alexa.appId}`);
    alexa.execute();
    //console.log(`Ending execution  for skill with APP_ID=${alexa.appId}`);
};
