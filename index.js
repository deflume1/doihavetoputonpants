// cache imports
const cacheConstructor = require('js-cache');
const cache = new cacheConstructor();
const EXPIRE_TIME = 60 * 60 * 1000; // 1 hour

// openweathermap-node imports from https://github.com/KwabenBerko/OpenWeatherMap-Node
const OpenWeatherMapHelper = require("./weather.js");
const weather = new OpenWeatherMapHelper(
    {
        APPID: '3987660f1d237e7ac31ea0e1ce013ac9',
        units: "imperial"
    }
);

const request = require("request");

// express imports
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
app.use('/', express.static('static'));

app.post('/welldoi', function(req, res){
    
    // get zipcode and country from post
    const zipcode = req.body.zipcode;
    const country = req.body.country;

    // if no zipcode, then bad request, and die
    if(!zipcode) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({ statusCode: 400, reason: 'Invalid zip code'}));
        return;
    }

    // check the cache first
    const cachedVal = cache.get(zipcode + country);
    if(cachedVal) {
         writeResponse(cachedVal, country, res);
         return;
    }

    // make the api call
    weather.getCurrentWeatherByZipCode(zipcode, country, (error, apiResponse) => {
        if(error){
            res.writeHead(503, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({ statusCode: 503, reason: 'Service temporarily unavailable'}));
        }
        else{
            handleResponse(apiResponse, zipcode, country, res);
        }
    });
});

/*
 * Handle the api response, cache it, and call the function to write it
 *
 * Example response from https://openweathermap.org/current
 *
 * {"coord":{"lon":-122.09,"lat":37.39},
    "sys":{"type":3,"id":168940,"message":0.0297,"country":"US","sunrise":1427723751,"sunset":1427768967},
    "weather":[{"id":800,"main":"Clear","description":"Sky is Clear","icon":"01n"}],
    "base":"stations",
    "main":{"temp":285.68,"humidity":74,"pressure":1016.8,"temp_min":284.82,"temp_max":286.48},
    "wind":{"speed":0.96,"deg":285.001},
    "clouds":{"all":0},
    "dt":1427700245,
    "id":0,
    "name":"Mountain View",
    "cod":200}
 */
function handleResponse(apiResponse, zipcode, country, res) {
    const temp = Number(apiResponse.main.temp);
    const windSpeed = Number(apiResponse.wind.speed);
    const val = {temp: temp, windSpeed: windSpeed};
    cache.set(zipcode + country, val, EXPIRE_TIME);
    writeResponse(val, country, res);
}

/*
 * Write the response to the client
 *
 */
function writeResponse(val, country, res) {
    const rawTemp = kelvinToFahrenheit(val.temp);
    const windSpeed = val.windSpeed;
    const windChillTemp = getWindChill(rawTemp, windSpeed);
    const windChill = precisionRound(rawTemp - windChillTemp, 2);
    const realTemp = windChillTemp;
    
    if(!country || country === 'us' || country === 'US') {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({temp: rawTemp, wind: windSpeed, windChill: windChill, realTemp: realTemp, units: 'imperial'}));
    } else {
        const rawTempC = fahrenheitToCelsius(rawTemp);
        const windSpeedKph = mphToKph(windSpeed);
        const windChillTempC = fahrenheitToCelsius(windChillTemp);
        const windChillC = precisionRound(rawTempC - windChillTempC, 2);
        const realTempC = fahrenheitToCelsius(windChillTemp);

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({temp: rawTempC, wind: windSpeedKph, windChill: windChillC, realTemp: realTempC, units: 'metric'}));
    }
}

/*
 * Convert kelvin to fahrenheit
 */
function kelvinToFahrenheit(degreesKelvin) {
    return precisionRound(degreesKelvin * 9/5 - 459.67, 2);
}

/*
 * Round to a specified precision
 */
function precisionRound(number, precision) {
    const factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

/*
 * Get the wind chill based on temp and windspeed
 * https://www.weather.gov/epz/wxcalc_windchill
 */
function getWindChill(temp, windspeed) {
    if(windspeed <= 3 || temp > 50) {
        return temp;
    }
        
    const scalingFactor = Math.pow(windspeed, 0.16);
    const result = 35.74 + (0.6215 * temp) - (35.75 * scalingFactor) + (0.4275 * temp * scalingFactor);
    return precisionRound(result, 2);
}

function fahrenheitToCelsius(degrees) {
    if(degrees <= 0) {
        return 0;
    }
    
    return precisionRound((degrees - 32) / 1.8, 2);
}

function mphToKph(mph) {
    return precisionRound(mph * 1.609344, 2);
}

const port = 36000;
app.listen(port);
console.log('Listening at http://localhost:' + port);
